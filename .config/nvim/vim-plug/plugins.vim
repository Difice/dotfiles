" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " File Explorer
    Plug 'scrooloose/NERDTree'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Nord Theme
    Plug 'arcticicestudio/nord-vim'
    " Lightline visuals
    Plug 'itchyny/lightline.vim'

call plug#end()

"Set colorscheme
colorscheme nord
"Set lightline colorscheme
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ }
"no showmode because lightline replaces it
set noshowmode
