#echo "|  _  \(_) / _|(_)             | | | |           | |          (_)      | |
#| | | | _ | |_  _   ___   ___  | | | | _ __ ___  | |__   _ __  _   ___ | |
#| | | || ||  _|| | / __| / _ \ | | | || '_ ` _ \ | '_ \ | '__|| | / _ \| |
#| |/ / | || |  | || (__ |  __/ | |_| || | | | | || |_) || |   | ||  __/| |
#|___/  |_||_|  |_| \___| \___|  \___/ |_| |_| |_||_.__/ |_|   |_| \___||_|"

pfetch

# Setting Colors
set -l foreground f8f8f2
set -l selection 44475a
set -l comment 267889
set -l red ff5555
set -l orange ffb86c
set -l yellow f1fa8c
set -l green 50fa7b
set -l purple bd93f9
set -l cyan 8be9fd
set -l pink ff79c6
set -l teal 339cb4

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_quote $yellow
set -g fish_color_g fish_color_param $teal
set -g fish_color_comment $comment
set -g fish_color_match --background=$selection
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment

alias vim='nvim'
alias ls='exa'
alias ll='ls -al'
alias stfu='echo "no u"'

#export LS_COLORS="di=1;36"

starship init fish | source


#function reload_gtk_theme() {
 # theme=$(gsettings get org.gnome.desktop.interface gtk-theme)
  #gsettings set org.gnome.desktop.interface gtk-theme ''
  #sleep 1
  #gsettings set org.gnome.desktop.interface gtk-theme $theme
#}
alias config='/usr/bin/git --git-dir=/home/user/.cfg/ --work-tree=/home/user'
